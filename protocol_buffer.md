# Protocol Buffer
## 레퍼런스
* [조대협의 블로그 :: 구글 프로토콜 버퍼 (Protocol buffer)](https://bcho.tistory.com/1182)
* [없으면 없는대로 :: 구글 Protocol Buffer 요약 (2017년 12월 기준)](https://ingeec.tistory.com/88)

## 개요
* 구글에서 개발하고 오픈소스로 공개한, 직렬화 데이타 구조(Serialized Data Structure)
	* 직렬화란 데이타를 파일로 저장하거나 또는 네트워크로 전송하기 위하여 바이너리 스트림 형태로 저장하는 행위
* Language-independent & platform-independent
* <프로토콜 버퍼>는 이제 구글의 데이터 공용어 (_gRPC의 디폴트 데이터 포맷_)
* JSON을 <프로토콜 버퍼>로 <프로토콜 버퍼>를 JSON으로 변환 가능
* XML보다 작고, 빠르고, 간단

## 구조
![](&&&SFLOCALFILEPATH&&&233E2635594F907222.png)

## address.proto
```
/syntax = “proto3”;/
/package com.terry.proto;/

/message Person{/
/string name = 1;/
/int32 age=2;/
/string email=3;/
/}/
```

## 컴파일
`protoc -I=./ —python_out=./ ./address.proto`

* -I에는 이 protofile이 있는 소스 디렉토리
* —python_out에는 생성된 파이썬 파일이 저장될 디렉토리
* 그리고 마지막으로 proto 파일을 정의한다. 

## write.py
```
/import address_pb2/

/person = address_pb2.Person()/

/person.name = ‘Terry’/
/person.age = 42/
/person.email = ‘terry@mycompany.com’/

/try:/
/f = open(‘myaddress’,’wb’)/
/f.write(person.SerializeToString())/
/f.close()/
/print ‘file is wriiten’/
/except IOError:/
/print ‘file creation error’/
```

## read.py
```
import address_pb2

person = address_pb2.Person()

try:
 f = open(‘myaddress’,’rb’)
 person.ParseFromString(f.read())
 f.close()
 print person.name
 print person.age
 print person.email
except IOError:
 print ‘file read error’
```

## 활용
이 기능을 사용하면, 클라이언트(모바일)에서 서버로 HTTP/JSON 과 같은 REST API를 구현할때, 전송전에, JSON을 프로토콜 버퍼 포맷으로 직렬화 해서, 전체적인 패킷양을 줄여서 전송하고, 서버에서는 받은 후에, 다시 JSON으로 풀어서 사용하는 구조를 취할 수 있다. 사실 이게 바로 GRPC 구조이다.
API 게이트웨이를 백앤드 서버 전면에 배치 해놓고, 프로토콜 버퍼로 들어온 메세지 바디를 JSON으로 변환해서 백앤드 API 서버에 넘겨주는 식의 구현이 가능하다.

#타다# #protocol buffer#