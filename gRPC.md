# gRPC
## 레퍼런스
* [조대협의 블로그 :: 구글의 HTTP 기반의 RPC 프로토콜 GRPC](https://bcho.tistory.com/1011?category=76068)
* [조대협의 블로그 :: grpc (google rpc)에 대한 분석 #1](https://bcho.tistory.com/1012?category=76068)
* [조대협의 블로그 :: grpc 설치하기](https://bcho.tistory.com/1013?category=76068)
* [조대협의 블로그 :: gRPC Channel](https://bcho.tistory.com/1294?category=76068)

## What is gRPC?
* 구글이 정의한 RPC
* 구글의 최신 API는 이제 REST API 뿐 아니라 gRPC API도 함께 제공함
* gRPC는 <프로토콜 버퍼>를 기본 데이터 시리얼라이즈 포맷으로 사용
 (but, JSON 등 다른 포맷도 사용 가능)
다양한 랭귀지 지원: C++, Java, Python, Go, Ruby, C#, Node.js, PHP, …

#타다# #gRPC#